#!/bin/bash
# Sets up a pi running fedora as a bluetooth sink with AptX support.

HOSTNAME=pibox3
TIMEZONE=Europe/Madrid
USERNAME=pi
PUBKEYFILE=id_rsa.pub

REMOVE_PACKAGES="audit abrt* firewalld rsyslog iscsi* nfs* samba* sssd* mlocate initial-setup"
ADD_PACKAGES="bash-completion vim-enhanced"

# set hostname and timezone
timedatectl set-timezone $TIMEZONE
hostnamectl set-hostname $HOSTNAME

# disable persistent logging
systemctl stop systemd-journald 2>/dev/null
rm -rf /var/log/journal

# disable audit
arch=$(uname -i)
if [[ $arch == aarch64 ]]; then
	# this is not effective...
	sed -i '/^kernelopts=/ s/$/ audit=0/' /boot/grub2/grubenv
elif [[ $arch == armv7hl ]]; then
	sed -i '/^\s*append/ s/$/ audit=0/' /boot/extlinux/extlinux.conf
	sed -i '/^\s*kernel/ s/$/ audit=0/' /boot/grub/grub.conf
fi

# try to speed up dnf a bit?
echo "fastestmirror=True" >> /etc/dnf/dnf.conf

# remove junk
if [[ -n $REMOVE_PACKAGES ]]; then
	dnf remove $REMOVE_PACKAGES -y
fi

# install rpmfusion and bluetooth/pulse packages
dnf install https://download1.rpmfusion.org/free/fedora/rpmfusion-free-release-$(rpm -E %fedora).noarch.rpm https://download1.rpmfusion.org/nonfree/fedora/rpmfusion-nonfree-release-$(rpm -E %fedora).noarch.rpm -y
dnf install bluez pulseaudio-module-bluetooth-freeworld pulseaudio-utils alsa-plugins-pulseaudio alsa-utils x265-devel x265-libs ffmpeg-devel ffmpeg-libs $ADD_PACKAGES -y

# create user
useradd --create-home --user-group $USERNAME
if [[ -r $PUBKEYFILE ]]; then
	mkdir -p /home/$USERNAME/.ssh
	cat $PUBKEYFILE >> /home/$USERNAME/.ssh/authorized_keys
	chown -R $USERNAME:$USERNAME /home/$USERNAME/.ssh
	chmod go-rwx -R /home/$USERNAME/.ssh
fi

# enable the pulseaudio service for the user
mkdir -p /home/$USERNAME/.config/systemd/user/default.target.wants/
mkdir -p /home/$USERNAME/.config/systemd/user/sockets.target.wants/
ln -s /usr/lib/systemd/user/pulseaudio.service /home/pi/.config/systemd/user/default.target.wants/pulseaudio.service
ln -s /usr/lib/systemd/user/pulseaudio.socket /home/pi/.config/systemd/user/sockets.target.wants/pulseaudio.socket
chown -R $USERNAME:$USERNAME /home/$USERNAME/.config

# setup autologin for the user
mkdir -p /etc/systemd/system/getty@tty1.service.d/
cat <<EOF > /etc/systemd/system/getty@tty1.service.d/override.conf
[Service]
ExecStart=
ExecStart=-/usr/sbin/agetty --autologin $USERNAME --noclear %I \$TERM
EOF
